package com.nelson.backend.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nelson.backend.dto.BookTripDTO;
import com.nelson.backend.model.BookTrip;
import com.nelson.backend.service.BookTripService;

@CrossOrigin(origins = "*")
@RequestMapping("bookTrip")
@RestController
public class BookTripController {

	private final BookTripService bookTripService;

	@Autowired
	public BookTripController(BookTripService bookTripService) {
		this.bookTripService = bookTripService;
	}
	@PostMapping
	public void addBookTrip(
			@Valid @RequestBody BookTripDTO bookTripDTO) {
		bookTripService.addBookTrip(bookTripDTO);
	}

	@GetMapping
	public List<BookTrip> getAllBookTrips() {
		return bookTripService.getAllBookTrips();
	}

	@GetMapping(path = "{id}")
	public BookTrip getBookTrip(@PathVariable("id") int id) {
		return bookTripService.getBookTrip(id).orElse(null);
	}

	@DeleteMapping(path = "{id}")
	public void deleteBookTrip(@PathVariable("id") int id) {
		bookTripService.deleteBookTrip(id);
	}

	@PutMapping(path = "{id}")
	public void updateBookTrip(@PathVariable("id") int id,
			@Valid @NonNull @RequestBody BookTripDTO bookTripDTO) {
		bookTripService.updateBookTrip(id, bookTripDTO);
	}
}

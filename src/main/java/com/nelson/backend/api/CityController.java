package com.nelson.backend.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nelson.backend.dto.CityDTO;
import com.nelson.backend.model.City;
import com.nelson.backend.service.CityService;

@CrossOrigin(origins = "*")
@RequestMapping("city")
@RestController
public class CityController {

	private final CityService cityService;

	@Autowired
	public CityController(CityService cityService) {
		this.cityService = cityService;
	}
	@PostMapping
	public void addCity(@Valid @NonNull @RequestBody CityDTO cityDTO) {
		cityService.addCity(cityDTO);
	}

	@GetMapping
	public List<City> getAllCities() {
		return cityService.getAllCities();
	}

	@GetMapping(path = "{id}")
	public City getCity(@PathVariable("id") int id) {
		return cityService.getCity(id).orElse(null);
	}

	@DeleteMapping(path = "{id}")
	public void deleteCity(@PathVariable("id") int id) {
		cityService.deleteCity(id);
	}

	@PutMapping(path = "{id}")
	public void updateCity(@PathVariable("id") int id,
			@Valid @NonNull @RequestBody CityDTO cityDTO) {
		cityService.updateCity(id, cityDTO);
	}
}

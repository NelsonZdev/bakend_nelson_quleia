package com.nelson.backend.api;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nelson.backend.dto.TouristDTO;
import com.nelson.backend.model.Tourist;
import com.nelson.backend.service.TouristService;

@CrossOrigin(origins = "*")
@RequestMapping("tourist")
@RestController
public class TouristController {

	private final TouristService touristService;

	@Autowired
	public TouristController(TouristService touristService) {
		this.touristService = touristService;
	}
	@PostMapping
	public HttpStatus addTourist(@Valid @NonNull @RequestBody TouristDTO touristDTO) {
		touristService.addTourist(touristDTO);
		return HttpStatus.OK;
	}

	@GetMapping
	@DateTimeFormat(iso = ISO.DATE)
	public List<Tourist> getAllTourists() {
		return touristService.getAllTourists();
	}

	@GetMapping(path = "{id}")
	public Tourist getTourist(@PathVariable("id") int id) {
		return touristService.getTourist(id).orElse(null);
	}

	@DeleteMapping(path = "{id}")
	public void deleteTourist(@PathVariable("id") int id) {
		touristService.deleteTourist(id);
	}

	@PutMapping(path = "{id}")
	public HttpStatus updateTourist(@PathVariable("id") int id,
			@Valid @NonNull @RequestBody TouristDTO touristDTO) {		
		touristService.updateTourist(id, touristDTO);
		return HttpStatus.OK;
	}
}

package com.nelson.backend.dto;

import java.sql.Date;

import javax.persistence.Id;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookTripDTO {

	@Id
	private int id;
	@Min(value = 0)
	private int idCity;
	@Min(value = 0)
	private int idTourist;
	@DateTimeFormat(pattern = "yyyy-MMM-dd")
	private Date travelDate;
	private String registrationDate;

	public BookTripDTO() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(@JsonProperty("id") int id) {
		this.id = id;
	}

	public int getIdCity() {
		return idCity;
	}

	public void setIdCity(@JsonProperty("idCity") int idCity) {
		this.idCity = idCity;
	}

	public int getIdTourist() {
		return idTourist;
	}

	public void setIdTourist(@JsonProperty("idTourist") int idTourist) {
		this.idTourist = idTourist;
	}

	public Date getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(@JsonProperty("birdDate") Date travelDate) {
		this.travelDate = travelDate;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(@JsonProperty("registrationDate") String registrationDate) {
		this.registrationDate = registrationDate;
	}
}

package com.nelson.backend.dto;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CityDTO {
	@Id
	private int id;
	@NotBlank
	private String name;
	@NotBlank
	private String population;
	@NotBlank
	private String recomendedPlace;
	@NotBlank
	private String hotel;

	public CityDTO() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(@JsonProperty("id") int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(@JsonProperty("name") String name) {
		this.name = name;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(@JsonProperty("population") String population) {
		this.population = population;
	}

	public String getRecomendedPlace() {
		return recomendedPlace;
	}

	public void setRecomendedPlace(
			@JsonProperty("recomendedPlace") String recomendedPlace) {
		this.recomendedPlace = recomendedPlace;
	}

	public String getHotel() {
		return hotel;
	}

	public void setHotel(@JsonProperty("hotel") String hotel) {
		this.hotel = hotel;
	}

}

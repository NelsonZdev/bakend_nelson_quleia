package com.nelson.backend.dto;

import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TouristDTO {
	private int id;
	@NotBlank
	private String name;
	@NotBlank
	private String lastName;
	@DateTimeFormat(pattern = "yyyy-MMM-dd")
	private Date birdDate;
	@NotBlank
	private String iDNumber;
	@NotBlank
	private String iDType;
	@Min(value = 0)
	private int travelFrecuency;
	@Min(value = 0)
	private Double travelBudget;
	@Min(value = 0)
	private int destiny;
	@Min(value = 0)
	private int creditCard;

	public TouristDTO() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(@JsonProperty("ID") int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(@JsonProperty("name") String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(@JsonProperty("lastName") String lastName) {
		this.lastName = lastName;
	}

	public Date getBirdDate() {
		return birdDate;
	}

	public void setBirdDate(@JsonProperty("birdDate") Date birdDate) {
		this.birdDate = birdDate;
	}

	public String getiDNumber() {
		return iDNumber;
	}

	public void setiDNumber(@JsonProperty("iDNumber") String iDNumber) {
		this.iDNumber = iDNumber;
	}

	public String getiDType() {
		return iDType;
	}

	public void setiDType(@JsonProperty("iDType") String iDType) {
		this.iDType = iDType;
	}

	public int getTravelFrecuency() {
		return travelFrecuency;
	}

	public void setTravelFrecuency(
			@JsonProperty("travelFrecuency") int travelFrecuency) {
		this.travelFrecuency = travelFrecuency;
	}

	public Double getTravelBudget() {
		return travelBudget;
	}

	public void setTravelBudget(
			@JsonProperty("travelBudget") Double travelBudget) {
		this.travelBudget = travelBudget;
	}

	public int getDestiny() {
		return destiny;
	}

	public void setDestiny(@JsonProperty("destiny") int destiny) {
		this.destiny = destiny;
	}

	public int getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(@JsonProperty("creditCard") int creditCard) {
		this.creditCard = creditCard;
	}
	
	public int compareTo (TouristDTO touristDTO) {
		int compare = 0;
		if (id != touristDTO.getId()) {
			compare++;
		}
		if (!name.equals(touristDTO.getName())){
			compare++;
		}
		if (!lastName.equals(touristDTO.getLastName())) {
			compare++;
		}
		if (!birdDate.equals(touristDTO.getBirdDate())) {
			compare++;
		}
		if (!iDNumber.equals(touristDTO.getiDNumber())) {
			compare++;
		}
		if (!iDType.equals(touristDTO.getiDType())) {
			compare++;
		}
		if (travelFrecuency != touristDTO.travelFrecuency) {
			compare++;
		}
		if (!travelBudget.equals(touristDTO.getTravelBudget())) {
			compare++;
		}
		if (destiny != touristDTO.getDestiny()) {
			compare++;
		}
		if (creditCard != touristDTO.getCreditCard()) {
			compare++;
		}			
		return compare;
	}

	@Override
	public int hashCode() {
		return Objects.hash(birdDate, creditCard, destiny, iDNumber, iDType, id,
				lastName, name, travelBudget, travelFrecuency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TouristDTO)) {
			return false;
		}
		TouristDTO other = (TouristDTO) obj;
		return id == other.id &&
				Objects.equals(name, other.name) &&
				Objects.equals(lastName, other.lastName) &&
				Objects.equals(birdDate, other.birdDate) &&
				Objects.equals(iDNumber, other.iDNumber) &&
				Objects.equals(iDType, other.iDType) &&
				travelFrecuency == other.travelFrecuency &&
				Objects.equals(travelBudget, other.travelBudget) &&
				destiny == other.destiny &&
				creditCard == other.creditCard;				
	}	
}

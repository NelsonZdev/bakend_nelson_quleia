package com.nelson.backend.model;

import java.security.SecureRandom;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import com.nelson.backend.dto.BookTripDTO;

@Entity(name = "book_trip")
@Table(name = "book_trip")
public class BookTrip {
	@Column(name = "id")
	@Id
	private int id;
	@Column(name = "id_city")
	@Min(value = 0)
	private int idCity;
	@Column(name = "id_tourist")
	@Min(value = 0)
	private int idTourist;
	@Column(name = "travel_date")
	@DateTimeFormat(pattern = "yyyy-MMM-dd")
	private Date travelDate;
	@Column(name = "registration_date")
	private String registrationDate;

	public BookTrip() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCity() {
		return idCity;
	}

	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}

	public int getIdTourist() {
		return idTourist;
	}

	public void setIdTourist(int idTourist) {
		this.idTourist = idTourist;
	}

	public Date getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public BookTrip dtoToPersistant(BookTripDTO bookTripDTO) {
		BookTrip persistantBookTrip = new BookTrip();
		persistantBookTrip.setId(bookTripDTO.getId());
		persistantBookTrip.setIdCity(bookTripDTO.getIdCity());
		persistantBookTrip.setIdTourist(bookTripDTO.getIdTourist());
		persistantBookTrip.setTravelDate(bookTripDTO.getTravelDate());
		persistantBookTrip
				.setRegistrationDate(bookTripDTO.getRegistrationDate());
		return persistantBookTrip;
	}
	public int randomInt() {
		int maxNumber = 999999999;
		SecureRandom random = new SecureRandom();
		return random.nextInt(maxNumber);
	}
}

package com.nelson.backend.model;

import java.security.SecureRandom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.nelson.backend.dto.CityDTO;

@Entity(name = "city")
@Table(name = "city")
public class City {
	@Column(name = "id")
	@Id
	private int id;
	@Column(name = "name")
	@NotBlank
	private String name;
	@Column(name = "population")
	@NotBlank
	private String population;
	@Column(name = "recommended_tourist_place")
	@NotBlank
	private String recomendedPlace;
	@Column(name = "most_booked_hotel")
	@NotBlank
	private String hotel;

	public City() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getRecomendedPlace() {
		return recomendedPlace;
	}

	public void setRecomendedPlace(String recomendedPlace) {
		this.recomendedPlace = recomendedPlace;
	}

	public String getHotel() {
		return hotel;
	}

	public void setHotel(String hotel) {
		this.hotel = hotel;
	}

	public City dtoToPersistant(CityDTO cityDTO) {
		City persistantCity = new City();
		persistantCity.setId(cityDTO.getId());
		persistantCity.setName(cityDTO.getName());
		persistantCity.setPopulation(cityDTO.getPopulation());
		persistantCity.setRecomendedPlace(cityDTO.getRecomendedPlace());
		persistantCity.setHotel(cityDTO.getHotel());
		return persistantCity;
	}
	public int randomInt() {
		int maxNumber = 999999999;
		SecureRandom random = new SecureRandom();
		return random.nextInt(maxNumber);
	}
}

package com.nelson.backend.model;

import java.sql.Date;
import com.nelson.backend.dto.TouristDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "tourist")
@Table(name = "tourist")
public class Tourist {
	@Column(name = "id")
	@Id
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "date_of_bird")
	private Date birdDate;
	@Column(name = "id_number")
	private String iDNumber;
	@Column(name = "id_type")
	private String iDType;
	@Column(name = "travel_frecuency")
	private int travelFrecuency;
	@Column(name = "travel_budget")
	private Double travelBudget;
	@Column(name = "destiny")
	private int destiny;
	@Column(name = "credit_card")
	private int creditCard;

	public Tourist() {
		// Void constructor. Instantiate without parameters
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirdDate() {
		return birdDate;
	}

	public void setBirdDate(Date birdDate) {
		this.birdDate = birdDate;
	}

	public String getiDNumber() {
		return iDNumber;
	}

	public void setiDNumber(String iDNumber) {
		this.iDNumber = iDNumber;
	}

	public String getiDType() {
		return iDType;
	}

	public void setiDType(String iDType) {
		this.iDType = iDType;
	}

	public int getTravelFrecuency() {
		return travelFrecuency;
	}

	public void setTravelFrecuency(int travelFrecuency) {
		this.travelFrecuency = travelFrecuency;
	}

	public Double getTravelBudget() {
		return travelBudget;
	}

	public void setTravelBudget(Double travelBudget) {
		this.travelBudget = travelBudget;
	}

	public int getDestiny() {
		return destiny;
	}

	public void setDestiny(int destiny) {
		this.destiny = destiny;
	}

	public int getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(int creditCard) {
		this.creditCard = creditCard;
	}

	public Tourist dtoToPersistant(TouristDTO touristDTO) {
		Tourist persistantTourist = new Tourist();
		persistantTourist.setId(touristDTO.getId());
		persistantTourist.setName(touristDTO.getName());
		persistantTourist.setLastName(touristDTO.getLastName());
		persistantTourist.setBirdDate(touristDTO.getBirdDate());
		persistantTourist.setiDNumber(touristDTO.getiDNumber());
		persistantTourist.setiDType(touristDTO.getiDType());
		persistantTourist.setTravelFrecuency(touristDTO.getTravelFrecuency());
		persistantTourist.setTravelBudget(touristDTO.getTravelBudget());
		persistantTourist.setDestiny(touristDTO.getDestiny());
		persistantTourist.setCreditCard(touristDTO.getCreditCard());
		return persistantTourist;
	}
}

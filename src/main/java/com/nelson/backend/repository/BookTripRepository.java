package com.nelson.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.nelson.backend.model.BookTrip;


@Repository("MySQLBookTrip")
public interface BookTripRepository extends CrudRepository<BookTrip, Integer>{

}

package com.nelson.backend.repository;

import org.springframework.stereotype.Repository;
import com.nelson.backend.model.City;
import org.springframework.data.repository.CrudRepository;

@Repository("MySQLCity")
public interface CityRepository extends CrudRepository<City, Integer> {

}

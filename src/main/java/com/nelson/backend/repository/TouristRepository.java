package com.nelson.backend.repository;

import org.springframework.stereotype.Repository;
import com.nelson.backend.model.Tourist;
import org.springframework.data.repository.CrudRepository;

@Repository("MySQLTourist")
public interface TouristRepository extends CrudRepository<Tourist, Integer> {
	
}

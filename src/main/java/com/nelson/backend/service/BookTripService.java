package com.nelson.backend.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.nelson.backend.dto.BookTripDTO;
import com.nelson.backend.model.BookTrip;
import com.nelson.backend.repository.BookTripRepository;

@Service
public class BookTripService {

	private BookTripRepository bookTripRepository;

	@Autowired
	public BookTripService(
			@Qualifier("MySQLBookTrip") BookTripRepository bookTripRepository) {
		this.bookTripRepository = bookTripRepository;
	}

	public int addBookTrip(BookTripDTO bookTripDTO) {
		BookTrip persistanBookTrip = new BookTrip();
		// Add actual time to register in bookTrip
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		bookTripDTO.setRegistrationDate(dateTimeFormatter.format(now));
		//Send
		bookTripRepository.save(persistanBookTrip.dtoToPersistant(bookTripDTO));
		return 0;
	}

	public List<BookTrip> getAllBookTrips() {
		return (List<BookTrip>) bookTripRepository.findAll();
	}

	public Optional<BookTrip> getBookTrip(int id) {
		return bookTripRepository.findById(id);
	}

	public int deleteBookTrip(int id) {
		bookTripRepository.deleteById(id);
		return 0;
	}

	public int updateBookTrip(int id, BookTripDTO bookTripDTO) {
		BookTrip persistantBookTrip = new BookTrip();
		bookTripRepository.findById(id);
		bookTripRepository
				.save(persistantBookTrip.dtoToPersistant(bookTripDTO));
		return 0;
	}
}

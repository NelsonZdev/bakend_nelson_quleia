package com.nelson.backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.nelson.backend.dto.CityDTO;
import com.nelson.backend.model.City;
import com.nelson.backend.repository.CityRepository;

@Service
public class CityService {

	private CityRepository cityRepository;

	@Autowired
	public CityService(@Qualifier("MySQLCity") CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}

	public int addCity(CityDTO cityDTO) {
		City persistantCity = new City();
		cityDTO.setId(persistantCity.randomInt());
		cityRepository.save(persistantCity.dtoToPersistant(cityDTO));
		return 0;
	}

	public List<City> getAllCities() {
		return (List<City>) cityRepository.findAll();
	}

	public Optional<City> getCity(int id) {
		return cityRepository.findById(id);
	}

	public int deleteCity(int id) {
		cityRepository.deleteById(id);
		return 0;
	}

	public int updateCity(int id, CityDTO cityDTO) {
		City persistantCity = new City();
		cityRepository.findById(id);
		cityRepository.save(persistantCity.dtoToPersistant(cityDTO));
		return 0;
	}
}

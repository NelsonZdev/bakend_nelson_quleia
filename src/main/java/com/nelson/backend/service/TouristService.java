package com.nelson.backend.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import com.nelson.backend.dto.TouristDTO;
import com.nelson.backend.model.Tourist;
import com.nelson.backend.repository.TouristRepository;
import com.nelson.backend.utilts.CustomLogger;

@Service
public class TouristService {

	private final TouristRepository touristRepository;
	private CustomLogger logger = new CustomLogger(TouristService.class.getName());

	@Autowired
	public TouristService(
			@Qualifier("MySQLTourist") TouristRepository touristRepository) {
		this.touristRepository = touristRepository;
	}

	public int addTourist(TouristDTO touristDTO) {
		try {
			Tourist persistantTourist = new Tourist();
			touristRepository.save(persistantTourist.dtoToPersistant(touristDTO));
			return 0;
		} catch (DataIntegrityViolationException e) {
			logger.errorLog(e.getMessage());
			return 1;
		}		
	}

	public List<Tourist> getAllTourists() {
		return (List<Tourist>) touristRepository.findAll();	
	}

	public Optional<Tourist> getTourist(int id) {
		return touristRepository.findById(id);
	}

	public int deleteTourist(int id) {
		try {
			touristRepository.deleteById(id);
			return 0;
		} catch (EmptyResultDataAccessException e) {
			logger.errorLog(e.getMessage());
			return 1;
		}
		
	}

	public int updateTourist(int id, TouristDTO touristDTO) {
		try {
			Tourist persistantTourist = new Tourist();
			boolean date = touristRepository.findById(id).isEmpty();
			if (!date) {
				touristRepository.save(persistantTourist.dtoToPersistant(touristDTO));
				return 0;
			}		
			else {
				throw new AssertionError("Cant find: " + id + " in tourist Database");
			}
		} catch (AssertionError e) {
			logger.errorLog(e.getMessage());
			return 1;
		}	
	}
}

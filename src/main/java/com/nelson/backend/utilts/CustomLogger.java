package com.nelson.backend.utilts;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class CustomLogger {

	private final Logger logger = Logger
			.getLogger(CustomLogger.class.getName());
	private FileHandler fileHandler = null;

	public CustomLogger(String nameOfFile) {
		try {
			fileHandler = new FileHandler("Logs/" + nameOfFile + ".log");
			fileHandler.setFormatter(new SimpleFormatter());
		} catch (SecurityException | IOException e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}

	public void infoLog(String message) {
		logger.log(Level.INFO, message);
	}
	public void warningLog(String message) {
		logger.log(Level.WARNING, message);
	}
	public void errorLog(String message) {
		logger.log(Level.SEVERE, message);
	}
}

package com.nelson.backend;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BackendApplicationTests {

	@Test
	void contextLoads() {
		BackendApplication backendApplication = new BackendApplication();
		assertNotNull(" ObjectIsNull ", backendApplication);
	}	
}

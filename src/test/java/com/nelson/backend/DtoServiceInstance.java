package com.nelson.backend;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import com.nelson.backend.dto.TouristDTO;

public class DtoServiceInstance {
	
	int idDemo;
	
	public int getIdDemo() {
		return idDemo;
	}
	
	public DtoServiceInstance() {
		idDemo = 1;
	}
	
	// Tourist DTO Default Instance
	public TouristDTO getTouristDTO() throws ParseException {
		TouristDTO touristDTO = new TouristDTO();
		touristDTO.setId(idDemo);
		touristDTO.setName("Demo");
		touristDTO.setLastName("Demo");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String date = "2000-01-01";
		Date demoDate = new Date(dateFormat.parse(date).getTime());
		touristDTO.setBirdDate(demoDate);
		
		touristDTO.setiDNumber("0");
		touristDTO.setiDType("CC");
		touristDTO.setTravelFrecuency(0);
		touristDTO.setTravelBudget(0.0D);
		touristDTO.setDestiny(0);
		touristDTO.setCreditCard(0);
		return touristDTO;
	}	
	public TouristDTO getTouristDTO1() throws ParseException {
		TouristDTO touristDTO = new TouristDTO();
		touristDTO.setId(idDemo);
		touristDTO.setName("Demo2");
		touristDTO.setLastName("Demo2");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String date = "2000-01-02";
		Date demoDate = new Date(dateFormat.parse(date).getTime());
		touristDTO.setBirdDate(demoDate);
		
		touristDTO.setiDNumber("1");
		touristDTO.setiDType("TI");
		touristDTO.setTravelFrecuency(1);
		touristDTO.setTravelBudget(1.0D);
		touristDTO.setDestiny(1);
		touristDTO.setCreditCard(1);
		return touristDTO;
	}	
}

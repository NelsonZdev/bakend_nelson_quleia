package com.nelson.backend.test.api;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.nelson.backend.DtoServiceInstance;
import com.nelson.backend.dto.TouristDTO;
import com.nelson.backend.service.TouristService;
import com.nelson.backend.utilts.CustomLogger;

@EnableConfigurationProperties
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class TouristControllerTest {

	@Value("${spring.datasource.url}")
	String DB_URL;
	@Value("${spring.datasource.username}")
	String DB_USER;
	@Value("${spring.datasource.password}")
	String DB_USER_PASSWORD;
	@LocalServerPort
	Integer port;

	CustomLogger logger = new CustomLogger(TouristService.class.getName());
	final int idDemo;
	final TouristDTO demoTouristDTO;
	final TouristDTO demoTouristDTO1;
	final String success = "Success!!";
	final String error = "Error: ";
	final DtoServiceInstance dtoServiceInstance = new DtoServiceInstance();
	RestTemplate restTemplate;
	String baseUrl;

	public TouristControllerTest() throws ParseException {
		// TODO Auto-generated constructor stub
		idDemo = dtoServiceInstance.getIdDemo();
		demoTouristDTO = dtoServiceInstance.getTouristDTO();
		demoTouristDTO1 = dtoServiceInstance.getTouristDTO1();
		restTemplate = new RestTemplate();
	}
	@BeforeEach
	void Init() {
		baseUrl = "http://localhost:" + port + "/tourist";
	}
	// DB Configuration
	Connection dbConnection() throws ClassNotFoundException, SQLException {
		return DriverManager.getConnection(DB_URL, DB_USER, DB_USER_PASSWORD);
	}
	// HTTP
	HttpHeaders httpHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		return httpHeaders;
	}
	// Convert Data
	List<TouristDTO> dbDataToTouristDTO(ResultSet resultSet)
			throws SQLException {
		List<TouristDTO> listTouristDTOs = new ArrayList<TouristDTO>();
		while (resultSet.next()) {
			TouristDTO touristDTO = new TouristDTO();
			touristDTO.setId(resultSet.getInt("id"));
			touristDTO.setName(resultSet.getString("name"));
			touristDTO.setLastName(resultSet.getString("last_name"));
			touristDTO.setBirdDate(resultSet.getDate("date_of_bird"));
			touristDTO.setiDNumber(resultSet.getString("id_number"));
			touristDTO.setiDType(resultSet.getString("id_type"));
			touristDTO.setTravelFrecuency(resultSet.getInt("travel_frecuency"));
			touristDTO.setTravelBudget(resultSet.getDouble("travel_budget"));
			touristDTO.setDestiny(resultSet.getInt("destiny"));
			touristDTO.setCreditCard(resultSet.getInt("credit_card"));
			listTouristDTOs.add(touristDTO);
		}
		return listTouristDTOs;
	}
	String jsonOfArrayTouristDTO(List<TouristDTO> listOfTouristDTO)
			throws JsonProcessingException, JSONException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		return mapper.writeValueAsString(listOfTouristDTO);
	}
	String jsonOfTouristDTO(TouristDTO touristDTO)
			throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		return mapper.writeValueAsString(touristDTO);
	}
	// SQL Query
	ResultSet selectAllSQL(Statement statement) throws SQLException {
		return statement.executeQuery("SELECT * FROM tourist;");
	}
	ResultSet findByIdSQL(Statement statement) throws SQLException {
		return statement
				.executeQuery("SELECT * FROM tourist WHERE id=" + idDemo + ";");
	}
	void deleteTouristSQL(Statement statement) throws SQLException {
		statement.executeUpdate(
				"DELETE FROM `tourist` WHERE `tourist`.`id` = " + idDemo + ";");
	}
	// Verify methods
	void verifyEmptyDataFromDB(Connection connection, int countItemsDB)
			throws SQLException {
		if (countItemsDB != 0) {
			connection.close();
			throw new AssertionError("DB not empty");
		}
	}
	void verifyDataEqualityOfCounts(Connection connection, int countItemsDB,
			int dataCount, String message) throws SQLException {
		if (countItemsDB != dataCount) {
			connection.close();
			throw new AssertionError(message);
		}
	}
	String verifyDBDataIntegrity(List<TouristDTO> instanceDtos) {
		boolean dbDataIsEqualToDemo = demoTouristDTO
				.equals(instanceDtos.get(0));
		String messageTestString = (dbDataIsEqualToDemo)
				? success
				: "The data in DB is not equal to demo data insert";
		return messageTestString;
	}
	// Data Count
	int countDBData(ResultSet resultSet) throws SQLException {
		int count = 0;
		while (resultSet.next())
			count++;
		return count;
	}

	@Test
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void addTourist() {
		try {
			// Request to DataBase
			final Connection connection = dbConnection();
			final Statement statement = connection.createStatement();
			ResultSet resultSet = selectAllSQL(statement);
			int countDBElements = countDBData(resultSet);

			// Not Empty
			if (countDBElements != 0) {
				connection.close();
				throw new AssertionError("DB not empty");
			}

			// Request to API
			URI uri = new URI(baseUrl);
			HttpEntity<String> request = new HttpEntity<>(
					jsonOfTouristDTO(demoTouristDTO), httpHeaders());
			ResponseEntity<String> responseEntity = restTemplate.exchange(uri,
					HttpMethod.POST, request, String.class);
			// Verify
			resultSet = findByIdSQL(statement);
			final List<TouristDTO> instanceDtos = dbDataToTouristDTO(resultSet);
			countDBElements = instanceDtos.size();
			if (countDBElements != 1) {
				connection.close();
				throw new AssertionError("The API does not work properly!!!");
			}

			// Result
			assertEquals(demoTouristDTO, instanceDtos.get(0));
			assertEquals(200, responseEntity.getStatusCodeValue());
			connection.close();
		} catch (Exception e) {
			logger.errorLog(e.getMessage());
			assertEquals(success, "Error: " + e);
		}
	}

	@Test
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:inserttourists.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void listTourist() {
		try {
			// DB
			Connection connection = dbConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = selectAllSQL(statement);
			// Instance
			final List<TouristDTO> instanceDtos = dbDataToTouristDTO(resultSet);
			int dbDataCount = instanceDtos.size();
			// Verify
			if (dbDataCount != 3) {
				throw new AssertionError("The data has been altered!!! ");
			}
			// Compare Demo whit DB result
			for (int i = 0; i < instanceDtos.size(); i++) {
				demoTouristDTO.setId(i + 1);
				demoTouristDTO.setName("Demo" + (i + 1));
				demoTouristDTO.setLastName("Demo" + (i + 1));
				if (!demoTouristDTO.equals(instanceDtos.get(i))) {
					connection.close();
					throw new AssertionError("The data has been altered!!!");
				}
			}
			// Convert to JSON
			String dbJsonArray = jsonOfArrayTouristDTO(instanceDtos);

			// API
			URI uri = new URI(baseUrl);
			final ResponseEntity<String> result = restTemplate.getForEntity(uri,
					String.class);
			String data = result.getBody(); // Result
			int apiDataCount = new JSONArray(data).length(); // Length of data
																// from
																// JsonArray

			// Compare results (API and DB)
			if (dbDataCount != apiDataCount) {
				connection.close();
				throw new AssertionError(
						"The data count in database and api has not equal!");
			}
			Boolean apiDataIsEqualToDBData = JSONCompare
					.compareJSON(dbJsonArray, data, JSONCompareMode.STRICT)
					.passed();
			// Result
			connection.close();
			assertTrue("The data in database and api has not equal!",
					apiDataIsEqualToDBData);
		} catch (Exception e) {
			logger.errorLog(e.getMessage());
			assertEquals(success, "Error: " + e);
		}
	}
	@Test
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:inserttourist.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void removeTourist() {
		try {
			// DB
			Connection connection = dbConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = findByIdSQL(statement);
			int countDBElements = countDBData(resultSet);
			verifyDataEqualityOfCounts(connection, countDBElements, 1,
					"The data count in database not equal by @SQL(Script) inserted");
			// API
			URI uri = new URI(baseUrl + "/" + idDemo);
			restTemplate.delete(uri);
			// Verify
			resultSet = selectAllSQL(statement);
			countDBElements = countDBData(resultSet); // Length of data from DB
			connection.close();
			// Result
			assertEquals(0, countDBElements, "Data not removed from DB");
		} catch (Exception e) {
			logger.errorLog(e.getMessage());
			assertEquals(success, "Error: " + e);
		}
	}
	@Test
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:inserttourist.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "classpath:deletetourists.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void updateTourist() {
		try {
			// DB
			final Connection connection = dbConnection();
			final Statement statement = connection.createStatement();
			ResultSet resultSet = selectAllSQL(statement);
			final List<TouristDTO> olderInstanceDtos = dbDataToTouristDTO(
					resultSet);
			int countDBElements = olderInstanceDtos.size();
			verifyDataEqualityOfCounts(connection, countDBElements, 1,
					"The data count in database not equal by @SQL(Script) inserted");
			// API
			URI uri = new URI(baseUrl + "/" + idDemo);
			HttpEntity<String> request = new HttpEntity<>(
					jsonOfTouristDTO(demoTouristDTO1), httpHeaders());
			ResponseEntity<String> responseEntity = restTemplate.exchange(uri,
					HttpMethod.PUT, request, String.class);
			// DB
			resultSet = findByIdSQL(statement);
			final List<TouristDTO> newInstanceDtos = dbDataToTouristDTO(
					resultSet);
			countDBElements = newInstanceDtos.size();
			// Verify exist only one Data in DB
			verifyDataEqualityOfCounts(connection, countDBElements, 1,
					"The database count of data not equal update HTTP request");
			if (newInstanceDtos.get(0).getId() != idDemo) {
				connection.close();
				throw new AssertionError("ID in HTTP request has changed");
			}

			final int compareData = olderInstanceDtos.get(0)
					.compareTo(newInstanceDtos.get(0));
			assertEquals(9, compareData, "Error in HTTP request update");
			assertEquals(200, responseEntity.getStatusCodeValue());
			connection.close();
		} catch (Exception e) {
			logger.errorLog(e.getMessage());
			assertEquals(success, "Error: " + e);
		}
	}
}
